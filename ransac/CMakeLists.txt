cmake_minimum_required(VERSION 3.10)
project(CV_Practice_RANSAC_Line2D)

set(CMAKE_CXX_STANDARD 14)

add_executable(CV_Practice_RANSAC_Line2D
        CV_Practice_RANSAC_Line2D.cpp)

find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )
target_link_libraries( CV_Practice_RANSAC_Line2D ${OpenCV_LIBS} )
