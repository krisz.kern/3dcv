#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>

using namespace cv;
using namespace std;

#define WIDTH 800
#define HEIGHT 600
#define WINDOW "Squash Game"
#define LEFT_ARROW 65361
#define RIGHT_ARROW 65363
#define LEFT_BOUND 20
#define RIGHT_BOUND WIDTH - 20
#define UPPER_BOUND 20
#define RACKET_WIDTH 100
#define RACKET_HEIGHT 20
#define BALL_WIDTH 20
#define BALL_HEIGHT 20
#define RACKET_POSITION_Y 500

Mat image;

int life = 3;

int racketPosition = 350;

int ballPosition[2]{150, 200};
int ballDirection[2]{1, 1};

int mousePosition;

void drawFrame() {
    rectangle(image,
              Point(0, 0),
              Point(LEFT_BOUND, HEIGHT),
              Scalar(0, 128, 0), CV_FILLED);
    rectangle(image,
              Point(0, 0),
              Point(WIDTH, UPPER_BOUND),
              Scalar(0, 128, 0), CV_FILLED);
    rectangle(image,
              Point(RIGHT_BOUND, 0),
              Point(WIDTH, HEIGHT),
              Scalar(0, 128, 0), CV_FILLED);
}

void drawPlayField() {
    rectangle(image,
              Point(LEFT_BOUND, UPPER_BOUND),
              Point(RIGHT_BOUND, HEIGHT),
              Scalar(255, 255, 255), CV_FILLED);
    putText(image,
            to_string(life),
            Point(70, 70),
            FONT_HERSHEY_SIMPLEX,
            1,
            Scalar(255, 0, 0));
}

void drawBall() {
    rectangle(image,
              Point(ballPosition[0], ballPosition[1]),
              Point(ballPosition[0] + BALL_WIDTH, ballPosition[1] + BALL_HEIGHT),
              Scalar(0, 0, 255), CV_FILLED);
}

void drawRacket() {
    rectangle(image,
              Point(racketPosition, RACKET_POSITION_Y),
              Point(racketPosition + RACKET_WIDTH, RACKET_POSITION_Y + RACKET_HEIGHT),
              Scalar(255, 0, 0), CV_FILLED);
}

void moveRacket(int x) {
    racketPosition += x;

    if (racketPosition < LEFT_BOUND) {
        racketPosition = LEFT_BOUND;
    } else if (racketPosition > RIGHT_BOUND - RACKET_WIDTH) {
        racketPosition = RIGHT_BOUND - RACKET_WIDTH;
    }
}

void moveBall(int x, int y) {
    ballPosition[0] += x * ballDirection[0];
    ballPosition[1] += y * ballDirection[1];

    if (ballPosition[0] < LEFT_BOUND) {
        ballPosition[0] = LEFT_BOUND;
        ballDirection[0] = 1;
    } else if (ballPosition[0] > RIGHT_BOUND - BALL_WIDTH) {
        ballPosition[0] = RIGHT_BOUND - BALL_WIDTH;
        ballDirection[0] = -1;
    }

    int ballRacketYDifference = ballPosition[1] + BALL_HEIGHT - RACKET_POSITION_Y;
    if (ballPosition[1] < UPPER_BOUND) {
        ballPosition[1] = UPPER_BOUND;
        ballDirection[1] = 1;
    } else if (ballRacketYDifference > 0 && ballRacketYDifference < 15 &&
               ballPosition[0] + BALL_WIDTH > racketPosition &&
               ballPosition[0] < racketPosition + RACKET_WIDTH) {
        ballPosition[1] = RACKET_POSITION_Y - BALL_HEIGHT;
        ballDirection[1] = -1;
    } else if (ballPosition[1] > HEIGHT) {
        ballPosition[0] = ballPosition[1] = 100;
        ballDirection[0] = ballDirection[1] = 1;
        life--;
    }
}


void redraw() {

    drawPlayField();

    drawBall();

    drawRacket();

    if (life == 0) {
        putText(image,
                "Game Over",
                Point(200, 200),
                FONT_HERSHEY_SIMPLEX,
                2,
                Scalar(0, 0, 255),
                2);
    }

    imshow(WINDOW, image);
    if (life == 0) {
        waitKey();
        exit(0);
    }

}

void MouseCallBackFunc(int event, int x, int y, int flags, void *userdata) {
    if (event == EVENT_MOUSEMOVE) {

        if (mousePosition > x) {
            moveRacket(-5);
        } else {
            moveRacket(5);
        }
        mousePosition = x;

        redraw();
    }
}

int main(int argc, char **argv) {
    image = Mat::zeros(HEIGHT, WIDTH, CV_8UC3);


    namedWindow(WINDOW, WINDOW_AUTOSIZE);// Create a window for display.
    moveWindow(WINDOW, 200, 300);
    setMouseCallback(WINDOW, MouseCallBackFunc, nullptr);


    imshow(WINDOW, image);                   // Show our image inside it.

    drawFrame();

    int key;
    while (true) {
        key = waitKeyEx(40);
        moveBall(8, 8);

        if (key == 27) {
            break;
        }

        switch (key) {
            case 'a':
                moveRacket(-10);
                break;
            case 'd':
                moveRacket(10);
                break;
            case LEFT_ARROW:
                moveRacket(-10);
                break;
            case RIGHT_ARROW:
                moveRacket(10);
                break;
        }
        if (getWindowProperty(WINDOW, WND_PROP_AUTOSIZE) >= 0) {
            redraw();
        } else {
            break;
        }
    }

    return 0;
}
