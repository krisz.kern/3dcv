#include "stdafx.h"

#include <iostream>
#include <opencv\cv.hpp>
#include <opencv\highgui.h>
#include <fstream>
#include <vector>
#include <string>
#include <set>

#define _USE_MATH_DEFINES
#include <math.h>

using namespace cv;
using namespace std;

#define SAMPLE_SIZE 4
#define THRESHOLD 0.03
#define CONFIDENCE 0.99

struct Sphere {
	Point3d center;
	double radius;
};

vector<Point3d> readFile(string fileName)
{
	vector<Point3d> points;
	double x, y, z;
	ifstream file;
	file.open(fileName.c_str());
	string s;
	getline(file, s);
	while (file >> x && file >> y && file >> z)
	{
		points.emplace_back(Point3d(x, y, z));
	}
	file.close();
	return points;
}

void writeFile(string fileName, Sphere& sphere)
{
	ofstream file;
	file.open(fileName.c_str());
	
	for (size_t i = 0; i < 180; i++)
	{
		for (size_t j = 0; j < 360; j++)
		{
			file << (sphere.center.x + sphere.radius * sin(i * M_PI / 180) * cos(j * M_PI / 180)) << " "
				<< (sphere.center.y + sphere.radius * sin(i * M_PI / 180) * sin(j * M_PI / 180)) << " "
				<< (sphere.center.z + sphere.radius * cos(i * M_PI / 180)) << "\n";
		}
	}

	file.close();
}

double getRandomNumber()
{
	return static_cast<double>(rand()) / RAND_MAX;
}

size_t getIterationNumber(size_t inlierNumber, size_t pointNumber)
{

	const double one_minus_confidence = 1.0 - CONFIDENCE;
	const double log_confidence = log(one_minus_confidence);
	const double inlier_ratio = static_cast<double>(inlierNumber) / pointNumber;
	const double pow_inlier_ratio = std::pow(inlier_ratio, SAMPLE_SIZE);
	return log_confidence / log(1.0 - pow_inlier_ratio);
}

void fitSphereRANSAC(const vector<Point3d>* const points, vector<size_t>* inliers, Sphere& sphere)
{
	inliers->clear();
	vector<size_t> inliersTemp;
	inliersTemp.reserve(points->size());

	double D, E, F, G, T;

	Point3d P, Q, R, S, center;

	double t1, t2, t3, t4, radius;

	size_t * const samples = new size_t[SAMPLE_SIZE];
	size_t iterations = std::numeric_limits<size_t>::max();

	size_t i, j, randomIndex;

	for (i = 0; i < iterations; i++)
	{
		// select 4 points
		for (size_t sample_idx = 0; sample_idx < SAMPLE_SIZE; ++sample_idx)
		{
			// Select 4 points via its index randomly
			size_t idx = round(getRandomNumber() * (points->size() - 1));
			samples[sample_idx] = idx;

			// Check if the selected index has been already selected
			for (size_t prev_sample_idx = 0; prev_sample_idx < sample_idx; ++prev_sample_idx)
			{
				if (samples[prev_sample_idx] == samples[sample_idx])
				{
					--sample_idx;
					continue;
				}
			}
		}

		P = points->at(samples[0]);
		Q = points->at(samples[1]);
		R = points->at(samples[2]);
		S = points->at(samples[3]);

		t1 = -(P.x * P.x + P.y * P.y + P.z * P.z);
		t2 = -(Q.x * Q.x + Q.y * Q.y + Q.z * Q.z);
		t3 = -(R.x * R.x + R.y * R.y + R.z * R.z);
		t4 = -(S.x * S.x + S.y * S.y + S.z * S.z);

		double m0[4][4] = {
			{P.x, P.y, P.z, 1},
			{Q.x, Q.y, Q.z, 1},
			{R.x, R.y, R.z, 1},
			{S.x, S.y, S.z, 1}
		};
		T = determinant(Mat(4, 4, CV_64F, m0));

		double m1[4][4] = {
			{t1, P.y, P.z, 1},
			{t2, Q.y, Q.z, 1},
			{t3, R.y, R.z, 1},
			{t4, S.y, S.z, 1}
		};
		D = determinant(Mat(4, 4, CV_64F, m1)) / T;

		double m2[4][4] = {
			{P.x, t1, P.z, 1},
			{Q.x, t2, Q.z, 1},
			{R.x, t3, R.z, 1},
			{S.x, t4, S.z, 1}
		};
		E = determinant(Mat(4, 4, CV_64F, m2)) / T;

		double m3[4][4] = {
			{P.x, P.y, t1, 1},
			{Q.x, Q.y, t2, 1},
			{R.x, R.y, t3, 1},
			{S.x, S.y, t4, 1}
		};
		F = determinant(Mat(4, 4, CV_64F, m3)) / T;

		double m4[4][4] = {
			{P.x, P.y, P.z, t1},
			{Q.x, Q.y, Q.z, t2},
			{R.x, R.y, R.z, t3},
			{S.x, S.y, S.z, t4}
		};
		G = determinant(Mat(4, 4, CV_64F, m4)) / T;

		center.x = - D / 2;
		center.y = - E / 2;
		center.z = - F / 2;

		radius = sqrt(D * D + E * E + F * F - 4 * G) / 2;

		inliersTemp.clear();
		for (j = 0; j < points->size(); j++)
		{
			const double &x = points->at(j).x, &y = points->at(j).y, &z = points->at(j).z;
			const double distance = sqrt(pow((x - center.x), 2) + pow((y - center.y), 2) + pow((z - center.z), 2));
			// if distance from center minus radius is below a threshold, its an inlier
			if (abs(distance - radius) < THRESHOLD)
			{
				inliersTemp.emplace_back(j);
			}
		}

		if (inliersTemp.size() > inliers->size())
		{
			inliersTemp.swap(*inliers);
			sphere.center.x = center.x;
			sphere.center.y = center.y;
			sphere.center.z = center.z;
			sphere.radius = radius;

			iterations = getIterationNumber(inliers->size(), points->size());
		}
	}
	delete[] samples;
}

int _tmain(int argc, _TCHAR* argv[])
{
	string fileNames[] = { "Sphere1.xyz", "Sphere2.xyz", "Sphere3.xyz", "Sphere4.xyz" };

	Sphere fittedSphere;
	vector<size_t> inliers;
	for (string fileName : fileNames)
	{
		vector<Point3d> points = readFile(fileName);

		fitSphereRANSAC(&points, &inliers, fittedSphere);

		writeFile("output_" + fileName, fittedSphere);
	}

	return 0;
}
