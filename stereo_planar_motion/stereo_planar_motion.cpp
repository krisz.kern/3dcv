#include <fstream>
#include <iostream>
#include <opencv/cv.hpp>
#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#include <chrono>

using namespace std;
using namespace cv;

// Detecting point correspondences in two images
void detectFeatures(
        const Mat &image1_, // The source images
        const Mat &image2_, // The destination image
        vector<Point2d> &source_points_, // Points in the source image
        vector<Point2d> &destination_points_); // Points in the destination image

// A function estimating the fundamental matrix from point correspondences
// by RANSAC.
void ransacFundamentalMatrix(
        const vector<Point2d> &input_source_points_, // Points in the source image
        const vector<Point2d> &input_destination_points_, // Points in the destination image
        const vector<Point2d> &normalized_input_src_points_, // Normalized points in the source image
        const vector<Point2d> &normalized_input_destination_points_, // Normalized points in the destination image
        const Mat &T1_, // Normalizing transformation in the source image
        const Mat &T2_, // Normalizing transformation in the destination image
        Mat &fundamental_matrix_, // The estimated fundamental matrix
        vector<size_t> &inliers_, // The inliers of the fundamental matrix
        double confidence_, // The required confidence of RANSAC
        double threshold_); // The inlier-outlier threshold

// A function estimating the fundamental matrix from point correspondences
// by least-squares fitting.
void getFundamentalMatrixLSQ(
        const vector<Point2d> &source_points_, // Points in the source image
        const vector<Point2d> &destination_points_, // Points in the destination image
        Mat &fundamental_matrix_); // The estimated fundamental matrix

// A function decomposing the essential matrix to the projection matrices
// of the two cameras.
void getProjectionMatrices(
        const Mat &essential_matrix_, // The parameters of the essential matrix
        const Mat &K1_, // The intrinsic camera parameters of the source image
        const Mat &K2_, // The intrinsic camera parameters of the destination image
        const Mat &src_point_, // A point in the source image
        const Mat &dst_point_, // A point in the destination image
        Mat &projection_1_, // The projection matrix of the source image
        Mat &projection_2_); // The projection matrix of the destination image

// A function estimating the 3D point coordinates from a point correspondences
// from the projection matrices of the two observing cameras.
void linearTriangulation(
        const Mat &projection_1_, // The projection matrix of the source image
        const Mat &projection_2_, // The projection matrix of the destination image
        const Mat &src_point_, // A point in the source image
        const Mat &dst_point_, // A point in the destination image
        Mat &point3d_); // The estimated 3D coordinates

// Normalizing the point coordinates for the fundamental matrix estimation
void normalizePoints(
        const vector<Point2d> &input_source_points_, // Points in the source image
        const vector<Point2d> &input_destination_points_, // Points in the destination image
        vector<Point2d> &output_source_points_, // Normalized points in the source image
        vector<Point2d> &output_destination_points_, // Normalized points in the destination image
        Mat &T1_, // Normalizing transformation in the source image
        Mat &T2_); // Normalizing transformation in the destination image

// Return the iteration number of RANSAC given the inlier ratio and
// a user-defined confidence value.
int getIterationNumber(int point_number_, // The number of points
                       int inlier_number_, // The number of inliers
                       int sample_size_, // The sample size
                       double confidence_); // The required confidence

// Printing the time to the console
void printTimes(
        const chrono::time_point<chrono::system_clock> &start, // The starting time
        const chrono::time_point<chrono::system_clock> &end, // The current time
        const string &message); // The message to be written

// Visualize the effect of the point normalization
void checkEffectOfNormalization(const vector<Point2d> &source_points_,  // Points in the first image
                                const vector<Point2d> &destination_points_,   // Points in the second image
                                const vector<Point2d> &normalized_source_points_,  // Normalized points in the first image
                                const vector<Point2d> &normalized_destination_points_, // Normalized points in the second image
                                const Mat &T1_, // Normalizing transforMation in the first image
                                const Mat &T2_, // Normalizing transforMation in the second image
                                const vector<size_t> &inliers_); // The inliers of the fundamental matrix

int main(int argc, char* argv[])
{
    // Using time point and system_clock
    chrono::time_point<chrono::system_clock> start, end;

    // Load images
    Mat image1 = imread("assets/0095.jpg");
    Mat image2 = imread("assets/0097.jpg");

    // Detect features
    vector<Point2d> source_points, destination_points; // Point correspondences
    start = chrono::system_clock::now();
    detectFeatures(image1, // First image
                   image2, // Second image
                   source_points, // Points in the first image
                   destination_points); // Points in the second image
    end = chrono::system_clock::now();
    printTimes(start, end, "feature detection");

    // Normalize the coordinates of the point correspondences to achieve numerically stable results
    start = chrono::system_clock::now();
    Mat T1, T2; // Normalizing transformations
    vector<Point2d> normalized_source_points, normalized_destination_points; // Normalized point correspondences
    normalizePoints(source_points, // Points in the first image
                    destination_points,  // Points in the second image
                    normalized_source_points,  // Normalized points in the first image
                    normalized_destination_points, // Normalized points in the second image
                    T1, // Normalizing transformation in the first image
                    T2); // Normalizing transformation  in the second image
    end = chrono::system_clock::now();
    printTimes(start, end, "feature normalization");

    start = chrono::system_clock::now();
    Mat F; // The fundamental matrix
    vector<size_t> inliers; // The inliers of the fundamental matrix
    ransacFundamentalMatrix(source_points,  // Points in the first image
                            destination_points,   // Points in the second image
                            normalized_source_points,  // Normalized points in the first image
                            normalized_destination_points, // Normalized points in the second image
                            T1, // Normalizing transformation in the first image
                            T2, // Normalizing transformation in the second image
                            F, // The fundamental matrix
                            inliers, // The inliers of the fundamental matrix
                            0.99, // The required confidence in the results
                            1.0); // The inlier-outlier threshold
    end = chrono::system_clock::now();
    printTimes(start, end, "RANSAC");

    printf("Correspondence number after filtering = %zu\n", inliers.size());

    // Ablation study: normalization
    checkEffectOfNormalization(source_points,  // Points in the first image
                               destination_points,   // Points in the second image
                               normalized_source_points,  // Normalized points in the first image
                               normalized_destination_points, // Normalized points in the second image
                               T1, // Normalizing transformation in the first image
                               T2, // Normalizing transformation in the second image
                               inliers); // The inliers of the fundamental matrix

    // Calibration matrix
    start = chrono::system_clock::now();
    Mat K = (Mat_<double>(3, 3) <<
            795.11588, 0, 517.12973,
            0, 795.11588, 395.59665,
            0, 0, 1);

    // Essential matrix
    // TODO
    Mat E = K.t() * F * K; // Calculate the essential matrix from the fundamental and intrinsic calibration matrices

    // Decompose the essential matrix
    Mat P1, P2;
    Mat tmp_src_point = (Mat_<double>(3, 1) << source_points[inliers[0]].x, source_points[inliers[0]].y, 1);
    Mat tmp_dst_point = (Mat_<double>(3, 1) << destination_points[inliers[0]].x, destination_points[inliers[0]].y, 1);
    getProjectionMatrices(E, // Essential matrix
                          K, // Calibration matrix of the first image
                          K, // Calibration matrix of the second image
                          tmp_src_point, // A point in the first image
                          tmp_dst_point, // A point in the second image
                          P1,  // The estimated projection matrix in the first image
                          P2); // The estimated projection matrix in the second image
    end = chrono::system_clock::now();
    printTimes(start, end, "essential matrix decomposition");

    // Draw the points and the corresponding epipolar lines
    constexpr double resize_by = 4.0;
    Mat tmp_image1, tmp_image2;
    resize(image1, tmp_image1, Size(image1.cols / resize_by, image1.rows / resize_by));
    resize(image2, tmp_image2, Size(image2.cols / resize_by, image2.rows / resize_by));

    vector<KeyPoint> src_inliers(inliers.size()), dst_inliers(inliers.size());
    vector<DMatch> inlier_matches(inliers.size());
    ofstream out_file("output/triangulated_points.xyz");
    start = chrono::system_clock::now();
    for (auto inl_idx = 0; inl_idx < inliers.size(); ++inl_idx)
    {
        // Triangulate, i.e. get the 3D position, of each correspondence and save the coordinates to a file
        Mat point_3d;
        linearTriangulation(P1, // Projection matrix of the first camera
                            P2, // Projection matrix of the second camera
                            (Mat)source_points[inliers[inl_idx]], // Point in the first image
                            (Mat)destination_points[inliers[inl_idx]], // Point in the second image
                            point_3d); // Triangulated 3D point

        out_file << point_3d.at<double>(0) << " " <<
                 point_3d.at<double>(1) << " " <<
                 point_3d.at<double>(2) << endl;

        // Construct the Matches vector for the drawing
        src_inliers[inl_idx].pt = source_points[inliers[inl_idx]] / resize_by;
        dst_inliers[inl_idx].pt = destination_points[inliers[inl_idx]] / resize_by;
        inlier_matches[inl_idx].queryIdx = inl_idx;
        inlier_matches[inl_idx].trainIdx = inl_idx;
    }
    out_file.close();
    end = chrono::system_clock::now();
    printTimes(start, end, "triangulation");

    Mat out_image;
    drawMatches(tmp_image1, src_inliers, tmp_image2, dst_inliers, inlier_matches, out_image);

    imshow("Matches", out_image);
    waitKey(0);

    return 0;
}

void printTimes(const chrono::time_point<chrono::system_clock> &start,
                const chrono::time_point<chrono::system_clock> &end,
                const string &message)
{
    chrono::duration<double> elapsed_seconds = end - start;
    printf("Processing time of the %s was %f secs.\n", message.c_str(), elapsed_seconds.count());
}

void getProjectionMatrices(
        const Mat &essential_matrix_,
        const Mat &K1_,
        const Mat &K2_,
        const Mat &src_point_,
        const Mat &dst_point_,
        Mat &projection_1_,
        Mat &projection_2_)
{
    // The first projection Matrix
    projection_1_ = (Mat_<double>(3, 4) << K1_.at<double>(0, 0), K1_.at<double>(0, 1), K1_.at<double>(0, 2), 0,
            K1_.at<double>(1, 0), K1_.at<double>(1, 1), K1_.at<double>(1, 2), 0,
            K1_.at<double>(2, 0), K1_.at<double>(2, 1), K1_.at<double>(2, 2), 0);

    // Decompose the essential matrix
    // OpenCV function: decomposeEssentialMat(E, R1, R2, t);
    Mat rotation_1, rotation_2, translation;

    SVD svd(essential_matrix_, SVD::FULL_UV);

    if (determinant(svd.u) < 0)
        svd.u.col(2) *= -1.0;

    if (determinant(svd.vt) < 0)
        svd.vt.row(2) *= -1.0;

    Mat w = (Mat_<double>(3, 3) << 0, -1, 0,
            1, 0, 0,
            0, 0, 1);

    rotation_1 = svd.u * w * svd.vt;
    rotation_2 = svd.u * w.t() * svd.vt;

    translation = svd.u.col(2) / norm(svd.u.col(2));

    Mat P21 = K2_ * (Mat_<double>(3, 4) <<
                                                rotation_1.at<double>(0, 0), rotation_1.at<double>(0, 1), rotation_1.at<double>(0, 2), translation.at<double>(0),
            rotation_1.at<double>(1, 0), rotation_1.at<double>(1, 1), rotation_1.at<double>(1, 2), translation.at<double>(1),
            rotation_1.at<double>(2, 0), rotation_1.at<double>(2, 1), rotation_1.at<double>(2, 2), translation.at<double>(2));

    Mat P22 = K2_ * (Mat_<double>(3, 4) <<
                                                rotation_1.at<double>(0, 0), rotation_1.at<double>(0, 1), rotation_1.at<double>(0, 2), -translation.at<double>(0),
            rotation_1.at<double>(1, 0), rotation_1.at<double>(1, 1), rotation_1.at<double>(1, 2), -translation.at<double>(1),
            rotation_1.at<double>(2, 0), rotation_1.at<double>(2, 1), rotation_1.at<double>(2, 2), -translation.at<double>(2));

    Mat P23 = K2_ * (Mat_<double>(3, 4) <<
                                                rotation_2.at<double>(0, 0), rotation_2.at<double>(0, 1), rotation_2.at<double>(0, 2), translation.at<double>(0),
            rotation_2.at<double>(1, 0), rotation_2.at<double>(1, 1), rotation_2.at<double>(1, 2), translation.at<double>(1),
            rotation_2.at<double>(2, 0), rotation_2.at<double>(2, 1), rotation_2.at<double>(2, 2), translation.at<double>(2));

    Mat P24 = K2_ * (Mat_<double>(3, 4) <<
                                                rotation_2.at<double>(0, 0), rotation_2.at<double>(0, 1), rotation_2.at<double>(0, 2), -translation.at<double>(0),
            rotation_2.at<double>(1, 0), rotation_2.at<double>(1, 1), rotation_2.at<double>(1, 2), -translation.at<double>(1),
            rotation_2.at<double>(2, 0), rotation_2.at<double>(2, 1), rotation_2.at<double>(2, 2), -translation.at<double>(2));

    const Mat * const Ps = new Mat[4]{ P21, P22, P23, P24 };

    for (auto i = 0; i < 4; ++i)
    {
        Mat point_3d;
        linearTriangulation(projection_1_,
                            Ps[i],
                            src_point_,
                            dst_point_,
                            point_3d);

        Mat projected_point_1 = projection_1_ * point_3d;
        Mat projected_point_2 = Ps[i] * point_3d;

        // Check if the 3D point is in front of both cameras
        if (projected_point_1.at<double>(2) < 0 ||
            projected_point_2.at<double>(2) < 0)
            continue;

        projection_2_ = Ps[i];
        break;
    }
    delete[] Ps;
}

void linearTriangulation(
        const Mat &projection_1_,
        const Mat &projection_2_,
        const Mat &src_point_,
        const Mat &dst_point_,
        Mat &point3d_)
{
    Mat A(4, 3, CV_64F);
    Mat b(4, 1, CV_64F);
    auto *A_ptr = reinterpret_cast<double *>(A.data);
    auto *b_ptr = reinterpret_cast<double *>(b.data);

    const auto &u1 = src_point_.at<double>(0);
    const auto &v1 = src_point_.at<double>(1);
    const auto &u2 = dst_point_.at<double>(0);
    const auto &v2 = dst_point_.at<double>(1);

    *(A_ptr++) = projection_1_.at<double>(0, 0) - u1 * projection_1_.at<double>(2, 0);
    *(A_ptr++) = projection_1_.at<double>(0, 1) - u1 * projection_1_.at<double>(2, 1);
    *(A_ptr++) = projection_1_.at<double>(0, 2) - u1 * projection_1_.at<double>(2, 2);
    *(b_ptr++) = (projection_1_.at<double>(0, 3) - u1 * projection_1_.at<double>(2, 3));

    *(A_ptr++) = projection_1_.at<double>(1, 0) - v1 * projection_1_.at<double>(2, 0);
    *(A_ptr++) = projection_1_.at<double>(1, 1) - v1 * projection_1_.at<double>(2, 1);
    *(A_ptr++) = projection_1_.at<double>(1, 2) - v1 * projection_1_.at<double>(2, 2);
    *(b_ptr++) = (projection_1_.at<double>(1, 3) - v1 * projection_1_.at<double>(2, 3));

    *(A_ptr++) = projection_2_.at<double>(0, 0) - u2 * projection_2_.at<double>(2, 0);
    *(A_ptr++) = projection_2_.at<double>(0, 1) - u2 * projection_2_.at<double>(2, 1);
    *(A_ptr++) = projection_2_.at<double>(0, 2) - u2 * projection_2_.at<double>(2, 2);
    *(b_ptr++) = (projection_2_.at<double>(0, 3) - u2 * projection_2_.at<double>(2, 3));

    *(A_ptr++) = projection_2_.at<double>(1, 0) - v2 * projection_2_.at<double>(2, 0);
    *(A_ptr++) = projection_2_.at<double>(1, 1) - v2 * projection_2_.at<double>(2, 1);
    *(A_ptr) = projection_2_.at<double>(1, 2) - v2 * projection_2_.at<double>(2, 2);
    *(b_ptr) = (projection_2_.at<double>(1, 3) - v2 * projection_2_.at<double>(2, 3));

    b = -b;

    point3d_ = A.inv(DECOMP_SVD) * b;
    point3d_.push_back(1.0);
}

void normalizePoints(
        const vector<Point2d> &input_source_points_,
        const vector<Point2d> &input_destination_points_,
        vector<Point2d> &output_source_points_,
        vector<Point2d> &output_destination_points_,
        Mat &T1_,
        Mat &T2_)
{
    T1_ = Mat::eye(3, 3, CV_64F);
    T2_ = Mat::eye(3, 3, CV_64F);

    auto N = input_source_points_.size();
    output_source_points_.resize(N);
    output_destination_points_.resize(N);

    Point2d center_1(0, 0),
            center_2(0, 0);
    for (auto i = 0; i < N; ++i)
    {
        center_1 += input_source_points_[i];
        center_2 += input_destination_points_[i];
    }
    center_1 = center_1 * (1.0 / N);
    center_2 = center_2 * (1.0 / N);

    double mean_1 = 0,
            mean_2 = 0;
    for (auto i = 0; i < N; ++i)
    {
        output_source_points_[i] = input_source_points_[i] - center_1;
        output_destination_points_[i] = input_destination_points_[i] - center_2;

        mean_1 += norm(output_source_points_[i]);
        mean_2 += norm(output_destination_points_[i]);
    }

    mean_1 /= N;
    mean_2 /= N;

    double ratio_1 = sqrt(2) / mean_1,
            ratio_2 = sqrt(2) / mean_2;

    for (auto i = 0; i < N; ++i)
    {
        output_source_points_[i] = output_source_points_[i] * ratio_1;
        output_destination_points_[i] = output_destination_points_[i] * ratio_2;
    }

    T1_ = (Mat_<double>(3, 3) <<
                                  ratio_1, 0, 0,
            0, ratio_1, 0,
            0, 0, 1) *
          (Mat_<double>(3, 3) << 1, 0, -center_1.x,
                  0, 1, -center_1.y,
                  0, 0, 1);

    T2_ = (Mat_<double>(3, 3) << ratio_2, 0, 0,
            0, ratio_2, 0,
            0, 0, 1) *
          (Mat_<double>(3, 3) << 1, 0, -center_2.x,
                  0, 1, -center_2.y,
                  0, 0, 1);
}


// Visualize the effect of the point normalization
void checkEffectOfNormalization(const vector<Point2d> &source_points_,  // Points in the first image
                                const vector<Point2d> &destination_points_,   // Points in the second image
                                const vector<Point2d> &normalized_source_points_,  // Normalized points in the first image
                                const vector<Point2d> &normalized_destination_points_, // Normalized points in the second image
                                const Mat &T1_, // Normalizing transforMation in the first image
                                const Mat &T2_, // Normalizing transforMation in the second image
                                const vector<size_t> &inliers_) // The inliers of the fundamental matrix
{
    Mat F(3, 3, CV_64F), // Fundamental matrix without point normalization
            normalized_F(3, 3, CV_64F); // Normalized fundamental matrix
    const size_t inlier_number = inliers_.size();

    // Copy the inliers into a vector
    vector<Point2d> source_points,
            destination_points,
            normalized_source_points,
            normalized_destination_points;

    source_points.reserve(inlier_number);
    destination_points.reserve(inlier_number);
    normalized_source_points.reserve(inlier_number);
    normalized_destination_points.reserve(inlier_number);

    for (const size_t &point_idx : inliers_)
    {
        source_points.emplace_back(source_points_[point_idx]);
        destination_points.emplace_back(destination_points_[point_idx]);
        normalized_source_points.emplace_back(normalized_source_points_[point_idx]);
        normalized_destination_points.emplace_back(normalized_destination_points_[point_idx]);
    }

    // Estimate the fundamental matrix without normalization
    getFundamentalMatrixLSQ(source_points,
                            destination_points,
                            F);

    // Estimate the fundamental matrix with normalization
    getFundamentalMatrixLSQ(normalized_source_points,
                            normalized_destination_points,
                            normalized_F);

    // Denormalize the fundamental matrix
    normalized_F = T2_.t() * normalized_F * T1_;

    double average_error_normalized = 0.0,
            average_error_not_normalized = 0.0;

    // Calculate the average errors to the inliers
    for (size_t i = 0; i < inlier_number; ++i)
    {
        // Symmetric epipolar distance
        Mat pt1 = (Mat_<double>(3, 1) << source_points[i].x, source_points[i].y, 1);
        Mat pt2 = (Mat_<double>(3, 1) << destination_points[i].x, destination_points[i].y, 1);

        // Error of the not normalized fundamental matrix
        Mat a = normalized_F * pt1;
        a = a * (1.0 / sqrt(a.at<double>(0)*a.at<double>(0) + a.at<double>(1)*a.at<double>(1)));
        a = pt2.t() * a;

        Mat b = pt2.t() * normalized_F;
        b = b * (1.0 / sqrt(b.at<double>(0)*b.at<double>(0) + b.at<double>(1)*b.at<double>(1)));
        b = b * pt1;

        double dist = 0.5 * (abs(a.at<double>(0)) + abs(b.at<double>(0)));
        average_error_normalized += dist * dist;

        // Error of the not normalized fundamental matrix
        a = F * pt1;
        a = a * (1.0 / sqrt(a.at<double>(0)*a.at<double>(0) + a.at<double>(1)*a.at<double>(1)));
        a = pt2.t() * a;

        b = pt2.t() * F;
        b = b * (1.0 / sqrt(b.at<double>(0)*b.at<double>(0) + b.at<double>(1)*b.at<double>(1)));
        b = b * pt1;

        dist = 0.5 * (abs(a.at<double>(0)) + abs(b.at<double>(0)));
        average_error_not_normalized += dist * dist;
    }

    average_error_normalized = sqrt(average_error_normalized / inlier_number);
    average_error_not_normalized = sqrt(average_error_not_normalized / inlier_number);

    printf("RMSE with normalization = %f px.\n", average_error_normalized);
    printf("RMSE without normalization = %f px.\n", average_error_not_normalized);
}

int getIterationNumber(int point_number_,
                       int inlier_number_,
                       int sample_size_,
                       double confidence_)
{
    const double inlier_ratio = static_cast<float>(inlier_number_) / point_number_;

    static const double log1 = log(1.0 - confidence_);
    const double log2 = log(1.0 - pow(inlier_ratio, sample_size_));

    const int k = log1 / log2;
    if (k < 0)
        return INT_MAX;
    return k;
}

void ransacFundamentalMatrix(
        const vector<Point2d> &input_src_points_,
        const vector<Point2d> &input_destination_points_,
        const vector<Point2d> &normalized_input_src_points_,
        const vector<Point2d> &normalized_input_destination_points_,
        const Mat &T1_,
        const Mat &T2_,
        Mat &fundamental_matrix_,
        vector<size_t> &inliers_,
        double confidence_,
        double threshold_)
{
    // The so-far-the-best fundamental matrix
    Mat best_fundamental_matrix;
    // The number of correspondences
    const size_t point_number = input_src_points_.size();

    // Initializing the index pool from which the minimal samples are selected
    vector<size_t> index_pool(point_number);
    for (size_t i = 0; i < point_number; ++i)
        index_pool[i] = i;

    // The size of a minimal sample
    constexpr size_t sample_size = 7;
    // The minimal sample
    auto *mss = new size_t[sample_size];

    size_t maximum_iterations = numeric_limits<int>::max(), // The maximum number of iterations set adaptively when a new best model is found
            iteration_limit = 5000, // A strict iteration limit which mustn't be exceeded
            iteration = 0; // The current iteration number

    vector<Point2d> source_points(sample_size),
            destination_points(sample_size);

    while (iteration++ < MIN(iteration_limit, maximum_iterations))
    {

        for (auto sample_idx = 0; sample_idx < sample_size; ++sample_idx)
        {
            // Select a random index from the pool
            const size_t idx = round((rand() / (double)RAND_MAX) * (index_pool.size() - 1));
            mss[sample_idx] = index_pool[idx];
            index_pool.erase(index_pool.begin() + idx);

            // Put the selected correspondences into the point containers
            const size_t point_idx = mss[sample_idx];
            source_points[sample_idx] = normalized_input_src_points_[point_idx];
            destination_points[sample_idx] = normalized_input_destination_points_[point_idx];
        }

        // Estimate fundamental matrix
        Mat fundamental_matrix(3, 3, CV_64F);
        getFundamentalMatrixLSQ(source_points, destination_points, fundamental_matrix);
        fundamental_matrix = T2_.t() * fundamental_matrix * T1_; // Denormalize the fundamental matrix

        // Count the inliers
        vector<size_t> inliers;
        const double* p = (double *)fundamental_matrix.data;
        for (int i = 0; i < input_src_points_.size(); ++i)
        {
            // Symmetric epipolar distance
            Mat pt1 = (Mat_<double>(3, 1) << input_src_points_[i].x, input_src_points_[i].y, 1);
            Mat pt2 = (Mat_<double>(3, 1) << input_destination_points_[i].x, input_destination_points_[i].y, 1);

            Mat a = fundamental_matrix * pt1;
            a = a * (1.0 / sqrt(a.at<double>(0)*a.at<double>(0) + a.at<double>(1)*a.at<double>(1)));
            a = pt2.t() * a;

            Mat b = pt2.t() * fundamental_matrix;
            b = b * (1.0 / sqrt(b.at<double>(0)*b.at<double>(0) + b.at<double>(1)*b.at<double>(1)));
            b = b * pt1;

            double dist = 0.5 * (abs(a.at<double>(0)) + abs(b.at<double>(0)));

            if (dist < threshold_)
                inliers.push_back(i);
        }

        // Update if the new model is better than the previous so-far-the-best.
        if (inliers_.size() < inliers.size())
        {
            // Update the set of inliers
            inliers_.swap(inliers);
            inliers.clear();
            inliers.resize(0);
            // Update the model parameters
            best_fundamental_matrix = fundamental_matrix;
            // Update the iteration number
            maximum_iterations = getIterationNumber(point_number,
                                                    inliers_.size(),
                                                    sample_size,
                                                    confidence_);
        }

        // Put back the selected points to the pool
        for (size_t i = 0; i < sample_size; ++i)
            index_pool.push_back(mss[i]);
    }

    delete[] mss;

    fundamental_matrix_ = best_fundamental_matrix;
}

void getFundamentalMatrixLSQ(
        const vector<Point2d> &source_points_,
        const vector<Point2d> &destination_points_,
        Mat &fundamental_matrix_)
{
    const int sample_number = source_points_.size();
    Mat evals(1, 9, CV_64F), evecs(9, 9, CV_64F);
    Mat A(sample_number, 9, CV_64F);
    auto *A_ptr = reinterpret_cast<double *>(A.data);

    // form a linear system: i-th row of A(=a) represents
    // the equation: (m2[i], 1)'*F*(m1[i], 1) = 0
    for (auto i = 0; i < sample_number; i++)
    {
        const double x0 = source_points_[i].x, y0 = source_points_[i].y;
        const double x1 = destination_points_[i].x, y1 = destination_points_[i].y;

        *(A_ptr++) = x1 * x0;
        *(A_ptr++) = x1 * y0;
        *(A_ptr++) = x1;
        *(A_ptr++) = y1 * x0;
        *(A_ptr++) = y1 * y0;
        *(A_ptr++) = y1;
        *(A_ptr++) = x0;
        *(A_ptr++) = y0;
        *(A_ptr++) = 1;
    }

    // A*(f11 f12 ... f33)' = 0 is singular (7 equations for 9 variables), so
    // the solution is linear subspace of dimensionality 2.
    // => use the last two singular vectors as a basis of the space
    // (according to SVD properties)
    Mat cov = A.t() * A;
    eigen(cov, evals, evecs);

    auto *F_ptr = reinterpret_cast<double*>(fundamental_matrix_.data);
    memcpy(fundamental_matrix_.data, evecs.row(8).data, 9 * sizeof(double));
}

void detectFeatures(
        const Mat &image1_,
        const Mat &image2_,
        vector<Point2d> &source_points_,
        vector<Point2d> &destination_points_)
{
    printf("Detect SIFT features\n");
    Mat descriptors1, descriptors2; // The descriptors of the found keypoints in the two images
    vector<KeyPoint> keypoints1, keypoints2; // The keypoints in the two images

    Ptr<xfeatures2d::SIFT> detector = xfeatures2d::SIFT::create(); // The SIFT detector
    detector->detect(image1_, keypoints1); // Detecting keypoints in the first image
    detector->compute(image1_, keypoints1, descriptors1); // Computing the descriptors of the keypoints in the first image
    printf("Features found in the first image: %zu\n", keypoints1.size());

    detector->detect(image2_, keypoints2); // Detecting keypoints in the second image
    detector->compute(image2_, keypoints2, descriptors2); // Computing the descriptors of the keypoints in the second image
    printf("Features found in the second image: %zu\n", keypoints2.size());

    // Do the descriptor matching by an approximated k-nearest-neighbors algorithm (FLANN) with k = 2.
    vector<vector< DMatch >> matches_vector;
    FlannBasedMatcher matcher(new flann::KDTreeIndexParams(5), new flann::SearchParams(32));
    matcher.knnMatch(descriptors1, descriptors2, matches_vector, 2);

    // Iterate through the matches, apply the SIFT ratio test and if the match passes,
    // add it to the vector of found correspondences
    for (auto m : matches_vector)
    {
        if (m.size() == 2 && m[0].distance < m[1].distance * 0.80)
        {
            auto& kp1 = keypoints1[m[0].queryIdx];
            auto& kp2 = keypoints2[m[0].trainIdx];
            source_points_.push_back(kp1.pt);
            destination_points_.push_back(kp2.pt);
        }
    }
    printf("Detected correspondence number: %zu\n", source_points_.size());
}