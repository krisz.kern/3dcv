#include <opencv/cv.hpp>
#include <opencv/highgui.h>
#include <fstream>
#include <vector>
#include <string>

using namespace cv;
using namespace std;

#define SAMPLE_SIZE 3
#define NOISE_POINTS 50
#define THRESHOLD 5
#define CONFIDENCE 0.99
#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 960

struct Circle {
    Point2d center;
    double radius = 0;
};

vector<Point2d> readFile(const string& fileName)
{
    vector<Point2d> points;
    size_t x, y;
    ifstream file(fileName);
    string line;
    getline(file, line);
    while (file >> x && file >> y)
    {
        // divide by 2 because we're using half of original window size to fit into a smaller screen
        points.emplace_back(Point2d(x / 2, y / 2));
    }
    file.close();
    return points;
}


double getRandomNumber()
{
    return static_cast<double>(rand()) / RAND_MAX;
}

void addNoise(vector<Point2d>& points)
{
    for (size_t i = 0; i < NOISE_POINTS; i++)
    {
        points.emplace_back(Point2d(getRandomNumber() * (WINDOW_WIDTH - 1), getRandomNumber() * (WINDOW_HEIGHT - 1)));
    }
}

// draw points into an image
void drawPoints(const vector<Point2d>* points, Mat &image, const Scalar& color, const vector<size_t>* inliers)
{
    if (inliers == nullptr)
    {
        for (const auto & point : *points)
        {
            circle(image, point, 2, color, 1);
        }
    }
    else
    {
        for (unsigned long inlier : *inliers)
        {
            circle(image, points->at(inlier), 2, color, 1);
        }
    }

}

size_t getIterationNumber(size_t inlierNumber, size_t pointNumber)
{

    const double one_minus_confidence = 1.0 - CONFIDENCE;
    const double log_confidence = log(one_minus_confidence);
    const double inlier_ratio = static_cast<double>(inlierNumber) / pointNumber;
    const double pow_inlier_ratio = std::pow(inlier_ratio, SAMPLE_SIZE);
    return log_confidence / log(1.0 - pow_inlier_ratio);
}

// Function to find the line given two points
void lineFromPoints(Point2d P, Point2d Q, double &a, double &b, double &c)
{
    a = Q.y - P.y;
    b = P.x - Q.x;
    c = a * (P.x) + b * (P.y);
}

// Function which converts the input line to its
// perpendicular bisector. It also inputs the points
// whose mid-point lies on the bisector
void perpendicularBisectorFromLine(const Point2d& P, const Point2d& Q, double &a, double &b, double &c)
{
    Point2d midPoint((P.x + Q.x) / 2, (P.y + Q.y) / 2);

    // c = -bx + ay
    c = -b * (midPoint.x) + a * (midPoint.y);

    double temp = a;
    a = -b;
    b = temp;
}

// Returns the intersection point of two lines
Point2d lineLineIntersection(double a1, double b1, double c1, double a2, double b2, double c2)
{
    double determinant = a1 * b2 - a2 * b1;
    if (determinant == 0)
    {
        // The lines are parallel. This is simplified
        // by returning a pair of FLT_MAX
        return Point2d(FLT_MAX, FLT_MAX);
    }

    else
    {
        double x = (b2*c1 - b1 * c2) / determinant;
        double y = (a1*c2 - a2 * c1) / determinant;
        return Point2d(x, y);
    }
}

void fitCircleRANSAC(const vector<Point2d>* const points, vector<size_t>* inliers, Circle& circle)
{
    inliers->clear();
    vector<size_t> inliersTemp;
    inliersTemp.reserve(points->size());

    Point2d P, Q, R, center;

    double a1, b1, c1, a2, b2, c2, radius;

    auto * const samples = new size_t[SAMPLE_SIZE];
    size_t iterations = std::numeric_limits<size_t>::max();

    size_t i, j, randomIndex;

    for (i = 0; i < iterations; i++)
    {
        // select 3 points
        for (size_t sample_idx = 0; sample_idx < SAMPLE_SIZE; ++sample_idx)
        {
            // Select a points via its index randomly
            size_t idx = round(getRandomNumber() * (points->size() - 1));
            samples[sample_idx] = idx;

            // Check if the selected index has been already selected
            for (size_t prev_sample_idx = 0; prev_sample_idx < sample_idx; ++prev_sample_idx)
            {
                if (samples[prev_sample_idx] == samples[sample_idx])
                {
                    --sample_idx;
                    continue;
                }
            }
        }

        P = points->at(samples[0]);
        Q = points->at(samples[1]);
        R = points->at(samples[2]);

        lineFromPoints(P, Q, a1, b1, c1);
        lineFromPoints(Q, R, a2, b2, c2);

        perpendicularBisectorFromLine(P, Q, a1, b1, c1);
        perpendicularBisectorFromLine(Q, R, a2, b2, c2);

        center = lineLineIntersection(a1, b1, c1, a2, b2, c2);

        radius = sqrt(pow((R.x - center.x), 2) + pow((R.y - center.y), 2));

        inliersTemp.clear();
        for (j = 0; j < points->size(); j++)
        {
            const double &x = points->at(j).x, &y = points->at(j).y;
            const double distance = sqrt(pow((x - center.x), 2) + pow((y - center.y), 2));
            // if distance from center minus radius is below a threshold, its an inlier
            if (abs(distance - radius) < THRESHOLD)
            {
                inliersTemp.emplace_back(j);
            }
        }

        if (inliersTemp.size() > inliers->size())
        {
            inliersTemp.swap(*inliers);
            circle.center.x = center.x;
            circle.center.y = center.y;
            circle.radius = radius;

            iterations = getIterationNumber(inliers->size(), points->size());
        }
    }
    delete[] samples;
}

int main(int argc, char* argv[])
{
    string fileNames[] = { "assets/points1.txt", "assets/points2.txt", "assets/points3.txt", "assets/points4.txt" };

    Circle fittedCircle;
    vector<size_t> inliers;
    for (const string& fileName : fileNames)
    {
        Mat image = Mat::zeros(WINDOW_HEIGHT, WINDOW_WIDTH, CV_8UC3);
        vector<Point2d> points = readFile(fileName);
        addNoise(points);
        drawPoints(&points, image, Scalar(255, 255, 255), nullptr);
        imshow(fileName, image);
        waitKey(0);

        fitCircleRANSAC(&points, &inliers, fittedCircle);
        drawPoints(&points, image, Scalar(255, 0, 0), &inliers);
        circle(image, fittedCircle.center, fittedCircle.radius, Scalar(0, 0, 255), 2);
        imshow(fileName + " - Fit Circle RANSAC", image);
        waitKey(0);
    }

    waitKey(0);

    return 0;
}
