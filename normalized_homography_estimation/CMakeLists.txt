cmake_minimum_required(VERSION 3.10)
project(normalized_homography_estimation)

set(CMAKE_CXX_STANDARD 14)

add_executable(normalized_homography_estimation normalized_homography_estimation.cpp)

find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )
target_link_libraries( normalized_homography_estimation ${OpenCV_LIBS} )