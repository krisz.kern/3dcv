#include <opencv/cv.hpp>
#include <vector>
#include <opencv2/core/core.hpp>
#include <chrono>
#include <string>

using namespace std;
using namespace cv;

struct inputData {
    pair<string, string> filenames;
    vector<Point2f> firstPoints;
    vector<Point2f> secondPoints;
};

// Printing the time to the console
void printTimes(
        const chrono::time_point<chrono::system_clock> &start, // The starting time
        const chrono::time_point<chrono::system_clock> &end, // The current time
        const string &message); // The message to be written

void normalizePoints(vector<Point2f>& firstPoints, vector<Point2f>& secondPoints, Mat& T1, Mat& T2);

Mat calcHomography(const vector<Point2f>& firstPoints, const vector<Point2f>& secondPoints);

void transformImage(const Mat& original_image,
                    Mat& new_image,
                    const Mat& transformation,
                    const bool& is_perspective);

int main(int argc, char* argv[])
{
    // The point correspondences below were measured by hand using gimp
    inputData A, B, C;
    A.filenames = make_pair("20191016_141850.jpg", "IMG_20191016_141853.jpg");
    A.firstPoints = {
            Point2f(231, 588.5),
            Point2f(1688.5, 594),
            Point2f(330, 2497),
            Point2f(1589.5, 2469.5)
    };
    A.secondPoints = {
            Point2f(720.5, 951.5),
            Point2f(2024, 676.5),
            Point2f(753.5, 2986.5),
            Point2f(1947, 3404.5)
    };

    B.filenames = make_pair("20191016_141909.jpg", "IMG_20191016_141910.jpg");
    B.firstPoints = {
            Point2f(828, 354),
            Point2f(2394, 285),
            Point2f(843, 1137),
            Point2f(2280, 1578)
    };
    B.secondPoints = {
            Point2f(900, 1008),
            Point2f(2948, 1088),
            Point2f(920, 2180),
            Point2f(2924, 2024)
    };

    C.filenames = make_pair("20191016_141930.jpg", "IMG_20191016_141931.jpg");
    C.firstPoints = {
            Point2f(1074, 588),
            Point2f(2103, 72),
            Point2f(1002, 1620),
            Point2f(2085, 1800)
    };
    C.secondPoints = {
            Point2f(902, 1518),
            Point2f(2425.5, 1485),
            Point2f(924, 2959),
            Point2f(2535.5, 2854.5)
    };

    vector<inputData> input = { A, B, C };

    for (auto & inputData : input)
    {
        // Using time point and system_clock
        chrono::time_point<chrono::system_clock> start, end;

        printf("----\n");
        printf("%s <-> %s\n", inputData.filenames.first.c_str(), inputData.filenames.second.c_str());
        printf("----\n");
        // Load images
        Mat image1 = imread("assets/" + inputData.filenames.first);
        Mat image2 = imread("assets/" + inputData.filenames.second);

        // Normalize the coordinates of the point correspondences to achieve numerically stable results
        start = chrono::system_clock::now();
        Mat T1, T2; // Normalizing transforMations
        normalizePoints(inputData.firstPoints, inputData.secondPoints, T1,T2);
        end = chrono::system_clock::now();
        printTimes(start, end, "feature normalization");

        start = chrono::system_clock::now();
        //We normalize the data set, calculate the homography on the normalized set,
        //then revert it using the normalization matrices to the original images
        Mat H = calcHomography(inputData.firstPoints, inputData.secondPoints);
        Mat HNorm = T2.inv() * H * T1;
        //cout << "Normalized homography matrix: " << endl << HNorm << endl;
        end = chrono::system_clock::now();
        printTimes(start, end, "homography calculation");

        start = chrono::system_clock::now();
        Mat transformedImage = Mat::zeros(image2.size().height, image2.size().width, image2.type());
        transformImage(image1, transformedImage, HNorm, true);
        end = chrono::system_clock::now();
        printTimes(start, end, "image transformation of " + inputData.filenames.first);

        start = chrono::system_clock::now();
        imwrite("output/output_" + inputData.filenames.first, transformedImage);
        end = chrono::system_clock::now();
        printTimes(start, end, "writing the image file");

        start = chrono::system_clock::now();
        //We normalize the data set, calculate the homography on the normalized set,
        //then revert it using the normalization matrices to the original images
        H = calcHomography(inputData.secondPoints, inputData.firstPoints);
        HNorm = T1.inv() * H * T2;
        //cout << "Normalized homography matrix: " << endl << HNorm << endl;
        end = chrono::system_clock::now();
        printTimes(start, end, "homography calculation");

        start = chrono::system_clock::now();
        transformedImage = Mat::zeros(image1.size().height, image1.size().width, image1.type());
        transformImage(image2, transformedImage, HNorm, true);
        end = chrono::system_clock::now();
        printTimes(start, end, "image transformation of " + inputData.filenames.second);

        start = chrono::system_clock::now();
        imwrite("output/output_" + inputData.filenames.second, transformedImage);
        end = chrono::system_clock::now();
        printTimes(start, end, "writing the image file");
    }

    return 0;
}

void printTimes(const chrono::time_point<chrono::system_clock> &start,
                const chrono::time_point<chrono::system_clock> &end,
                const string &message)
{
    chrono::duration<double> elapsed_seconds = end - start;
    printf("Processing time of the %s was %f secs.\n", message.c_str(), elapsed_seconds.count());
}

void normalizePoints(vector<Point2f>& firstPoints, vector<Point2f>& secondPoints, Mat &T1, Mat &T2) {
    const double SQRT2 = sqrt(2);
    int pairCount = firstPoints.size();

    float x1Mean = 0.0f, y1Mean = 0.0f, x2Mean = 0.0f, y2Mean = 0.0f;
    for (int i = 0; i < pairCount; i++) {
        x1Mean += firstPoints[i].x;
        y1Mean += firstPoints[i].y;
        x2Mean += secondPoints[i].x;
        y2Mean += secondPoints[i].y;
    }
    x1Mean /= pairCount;
    y1Mean /= pairCount;
    x2Mean /= pairCount;
    y2Mean /= pairCount;


    float x1Spread = 0.0f, y1Spread = 0.0f, x2Spread = 0.0f, y2Spread = 0.0f;
    for (int i = 0; i < pairCount; i++) {
        x1Spread += pow((firstPoints[i].x - x1Mean), 2);
        y1Spread += pow((firstPoints[i].y - y1Mean), 2);
        x2Spread += pow((secondPoints[i].x - x2Mean), 2);
        y2Spread += pow((secondPoints[i].y - y2Mean), 2);
    }
    x1Spread /= pairCount;
    y1Spread /= pairCount;
    x2Spread /= pairCount;
    y2Spread /= pairCount;


    // Translate center of mass to origin
    Mat Tr1 = Mat::eye(3, 3, CV_32F);
    Tr1.at<float>(0, 2) = -x1Mean;
    Tr1.at<float>(1, 2) = -y1Mean;

    Mat Tr2 = Mat::eye(3, 3, CV_32F);
    Tr2.at<float>(0, 2) = -x2Mean;
    Tr2.at<float>(1, 2) = -y2Mean;

    // Scale so average point distance is sqrt(2)
    Mat Sc1 = Mat::eye(3, 3, CV_32F);
    Sc1.at<float>(0, 0) = SQRT2 / sqrt(x1Spread);
    Sc1.at<float>(1, 1) = SQRT2 / sqrt(y1Spread);

    Mat Sc2 = Mat::eye(3, 3, CV_32F);
    Sc2.at<float>(0, 0) = SQRT2 / sqrt(x2Spread);
    Sc2.at<float>(1, 1) = SQRT2 / sqrt(y2Spread);

    T1 = Sc1 * Tr1;
    T2 = Sc2 * Tr2;

    // Apply normalization to data points
    for (int i = 0; i < pairCount; i++) {
        firstPoints[i].x = SQRT2 * (firstPoints[i].x - x1Mean) / sqrt(x1Spread);
        firstPoints[i].y = SQRT2 * (firstPoints[i].y - y1Mean) / sqrt(y1Spread);
        secondPoints[i].x = SQRT2 * (secondPoints[i].x - x2Mean) / sqrt(x2Spread);
        secondPoints[i].y = SQRT2 * (secondPoints[i].y - y2Mean) / sqrt(y2Spread);
    }
}

Mat calcHomography(const vector<Point2f>& firstPoints, const vector<Point2f>& secondPoints)
{
    Mat A(2 * firstPoints.size(), 9, CV_32F);

    for (int i = 0; i < firstPoints.size(); i++)
    {
        float u1 = firstPoints[i].x;
        float v1 = firstPoints[i].y;

        float u2 = secondPoints[i].x;
        float v2 = secondPoints[i].y;

        A.at<float>(2 * i, 0) = u1;
        A.at<float>(2 * i, 1) = v1;
        A.at<float>(2 * i, 2) = 1.0f;
        A.at<float>(2 * i, 3) = 0.0f;
        A.at<float>(2 * i, 4) = 0.0f;
        A.at<float>(2 * i, 5) = 0.0f;
        A.at<float>(2 * i, 6) = -u2 * u1;
        A.at<float>(2 * i, 7) = -u2 * v1;
        A.at<float>(2 * i, 8) = -u2;

        A.at<float>(2 * i + 1, 0) = 0.0f;
        A.at<float>(2 * i + 1, 1) = 0.0f;
        A.at<float>(2 * i + 1, 2) = 0.0f;
        A.at<float>(2 * i + 1, 3) = u1;
        A.at<float>(2 * i + 1, 4) = v1;
        A.at<float>(2 * i + 1, 5) = 1.0f;
        A.at<float>(2 * i + 1, 6) = -v2 * u1;
        A.at<float>(2 * i + 1, 7) = -v2 * v1;
        A.at<float>(2 * i + 1, 8) = -v2;

    }

    Mat eigen_vectors(9, 9, CV_32F), eigen_values(9, 9, CV_32F);

    eigen(A.t() * A, eigen_values, eigen_vectors);

    Mat H(3, 3, CV_32F);

    for (int i = 0; i < 9; i++)
    {
        H.at<float>(i / 3, i % 3) = eigen_vectors.at<float>(8, i);
    }

    //Normalize:
    H = H * (1.0 / H.at<float>(2, 2));

    return H;
}

void transformImage(const Mat& original_image, Mat& new_image, const Mat& transformation, const bool& is_perspective)
{
    Mat inverse_transformation = transformation.inv();
    Mat pt(3, 1, CV_32F);
    Mat ptTransformed;

    for (size_t x = 0; x < new_image.cols; x++){
        for (size_t y = 0; y < new_image.rows; y++)
        {
            pt.at<float>(0, 0) = x;
            pt.at<float>(1, 0) = y;
            pt.at<float>(2, 0) = 1.0;

            ptTransformed = inverse_transformation * pt;
            if (is_perspective) {
                ptTransformed = (1.0 / ptTransformed.at<float>(2, 0)) * ptTransformed;
            }

            size_t new_x = round(ptTransformed.at<float>(0,0));
            size_t new_y = round(ptTransformed.at<float>(1,0));

            if (new_x >= 0 && new_x < original_image.cols && new_y >= 0 && new_y < original_image.rows)
            {
                new_image.at<Vec3b>(y, x) = original_image.at<Vec3b>(new_y, new_x);
            }
        }
    }
}