#include <iostream>
#include <opencv/cv.hpp>
#include <fstream>
#include <vector>
#include <string>
#include "kdtree/kdtree.h"

using namespace cv;
using namespace std;

#define ITERATIONS 100
#define ERROR_DIFFERENCE_THRESHOLD 10.0

void readFiles(pair<string, string>& filePair, vector<Point3f>& sourcePoints, vector<Point3f>& targetPoints)
{
    float x, y, z;
    ifstream file;
    file.open(filePair.first);
    while (file >> x && file >> y && file >> z) {
        sourcePoints.emplace_back(Point3f(x, y, z));
        file >> x && file >> y && file >> z; //RGB values
    }
    file.close();

    file.open(filePair.second);
    while (file >> x && file >> y && file >> z) {
        targetPoints.emplace_back(Point3f(x, y, z));
        file >> x && file >> y && file >> z; //RGB values
    }
    file.close();
}

float pointDistance(const Point3f& point1, const Point3f& point2) {
    return sqrt(pow(point1.x - point2.x, 2) + pow(point1.y - point2.y, 2) + pow(point1.z - point2.z, 2));
}

kdtree* createKDTree(const vector<Point3f>& targetPoints)
{
    struct kdtree* tree = kd_create( 3 );
    for (int i = 0; i < targetPoints.size(); i++)
    {
        int* indexPointer = new int(i);
        kd_insert3f(tree, targetPoints[i].x, targetPoints[i].y, targetPoints[i].z, indexPointer);
    }
    return tree;
}

// KDTree min search
void findClosestPoints(vector<Point3f>& sourcePoints, const vector<Point3f>& targetPoints, kdtree* tree,
                       vector<int>& pointCorrespondences, float& totalError)
{
    pointCorrespondences.clear();

    totalError = 0;
    for (auto & sourcePoint : sourcePoints)
    {
        struct kdres* results = kd_nearest3f(tree, sourcePoint.x, sourcePoint.y, sourcePoint.z);
        kd_res_end(results);
        int j = *(int *) kd_res_item_data(results);
        totalError += pointDistance(sourcePoint, targetPoints[j]);
        pointCorrespondences.emplace_back(j);
        kd_res_free(results);
    }
}

// linear min search
void findClosestPoints(const vector<Point3f>& sourcePoints, const vector<Point3f>& targetPoints,
        vector<int>& pointCorrespondences, float& totalError)
{
    pointCorrespondences.clear();

    totalError = 0;
    float minDistance, distance;
    int minIndex;
    for (const auto & sourcePoint : sourcePoints)
    {
        minDistance = FLT_MAX;
        for(int j = 0; j < targetPoints.size(); j++)
        {
            distance = pointDistance(sourcePoint, targetPoints[j]);
            if (distance < minDistance) {
                minDistance = distance;
                minIndex = j;
            }
        }
        totalError += minDistance;
        pointCorrespondences.emplace_back(minIndex);
    }
}

void icpStep2(vector<Point3f>&  sourcePoints, vector<Point3f>& targetPoints, vector<int>& pointCorrespondences)
{
    Mat T = Mat::eye(4, 4, CV_32F);
    int N = sourcePoints.size();
    Mat sourceCentroid(3, 1, CV_32F), targetCentroid(3, 1, CV_32F);
    Mat sp_tmp(N, 3, CV_32F);
    Mat tp_tmp(N, 3, CV_32F);
    //Find centroids
    for (int i = 0; i < N; i++)
    {
        Point3f sp = sourcePoints[i];
        Point3f tp = targetPoints[pointCorrespondences[i]];
        sourceCentroid.at<float>(0, 0) += sp.x;
        sourceCentroid.at<float>(1, 0) += sp.y;
        sourceCentroid.at<float>(2, 0) += sp.z;
        targetCentroid.at<float>(0, 0) += tp.x;
        targetCentroid.at<float>(1, 0) += tp.y;
        targetCentroid.at<float>(2, 0) += tp.z;

        sp_tmp.at<float>(i, 0) = sp.x;
        sp_tmp.at<float>(i, 1) = sp.y;
        sp_tmp.at<float>(i, 2) = sp.z;
        tp_tmp.at<float>(i, 0) = tp.x;
        tp_tmp.at<float>(i, 1) = tp.y;
        tp_tmp.at<float>(i, 2) = tp.z;
    }
    sourceCentroid /= N;
    targetCentroid /= N;

    for (int i = 0; i < N; i++)
    {
        sp_tmp.at<float>(i, 0) = sp_tmp.at<float>(i, 0) - sourceCentroid.at<float>(0, 0);
        sp_tmp.at<float>(i, 1) = sp_tmp.at<float>(i, 1) - sourceCentroid.at<float>(1, 0);
        sp_tmp.at<float>(i, 2) = sp_tmp.at<float>(i, 2) - sourceCentroid.at<float>(2, 0);
        tp_tmp.at<float>(i, 0) = tp_tmp.at<float>(i, 0) - targetCentroid.at<float>(0, 0);
        tp_tmp.at<float>(i, 1) = tp_tmp.at<float>(i, 1) - targetCentroid.at<float>(1, 0);
        tp_tmp.at<float>(i, 2) = tp_tmp.at<float>(i, 2) - targetCentroid.at<float>(2, 0);
    }
    Mat H = sp_tmp.t() * tp_tmp;

    Mat W(3, 3, CV_32F), U(3, 3, CV_32F), Vt(3, 3, CV_32F);
    SVD::compute(H, W, U, Vt);

    Mat R = Vt.t() * U.t();
    // cout << "Rotation matrix\n" << R << endl;

    Mat t = targetCentroid - R * sourceCentroid;
    // cout << "Translation vector\n" << t << endl;

    T.at<float>(0, 0) = R.at<float>(0, 0);
    T.at<float>(0, 1) = R.at<float>(0, 1);
    T.at<float>(0, 2) = R.at<float>(0, 2);
    T.at<float>(1, 0) = R.at<float>(1, 0);
    T.at<float>(1, 1) = R.at<float>(1, 1);
    T.at<float>(1, 2) = R.at<float>(1, 2);
    T.at<float>(2, 0) = R.at<float>(2, 0);
    T.at<float>(2, 1) = R.at<float>(2, 1);
    T.at<float>(2, 2) = R.at<float>(2, 2);

    T.at<float>(0, 3) = t.at<float>(0, 0);
    T.at<float>(1, 3) = t.at<float>(1, 0);
    T.at<float>(2, 3) = t.at<float>(2, 0);

    // cout << "Transformation matrix\n" << T << endl;

    for (int i = 0; i < N; i++)
    {
        Point3f sp = sourcePoints[i];
        Mat p(4, 1, CV_32F);
        p.at<float>(0, 0) = sp.x;
        p.at<float>(1, 0) = sp.y;
        p.at<float>(2, 0) = sp.z;
        p.at<float>(3, 0) = 1.0f;

        p = T * p;
        sourcePoints[i] = Point3f(p.at<float>(0, 0), p.at<float>(1, 0), p.at<float>(2, 0));
    }
}

void icpStep(vector<Point3f>&  sourcePoints, vector<Point3f>& targetPoints, vector<int>& pointCorrespondences)
{
    Point3f sourceCentroid, targetCentroid;

    //Find centroids
    for (int i = 0; i < sourcePoints.size(); i++)
    {
        sourceCentroid += sourcePoints[i];
        targetCentroid += targetPoints[pointCorrespondences[i]];
    }
    sourceCentroid /= int(sourcePoints.size());
    targetCentroid /= int(sourcePoints.size());

    Mat H = Mat::zeros(3, 3, CV_32F);
    Point3f T = targetCentroid - sourceCentroid;
    for (int i = 0; i < sourcePoints.size(); i++)
    {
        sourcePoints[i] += T;
        float x1 = sourcePoints[i].x;
        float y1 = sourcePoints[i].y;
        float z1 = sourcePoints[i].z;
        float x2 = targetPoints[pointCorrespondences[i]].x;
        float y2 = targetPoints[pointCorrespondences[i]].y;
        float z2 = targetPoints[pointCorrespondences[i]].z;

        H.at<float>(0, 0) += x1 * x2;
        H.at<float>(0, 1) += x1 * y2;
        H.at<float>(0, 2) += x1 * z2;
        H.at<float>(1, 0) += y1 * x2;
        H.at<float>(1, 1) += y1 * y2;
        H.at<float>(1, 2) += y1 * z2;
        H.at<float>(2, 0) += z1 * x2;
        H.at<float>(2, 1) += z1 * y2;
        H.at<float>(2, 2) += z1 * z2;
    }
    Mat W(3, 3, CV_32F), U(3, 3, CV_32F), Vt(3, 3, CV_32F);
    SVDecomp(H, W, U, Vt);
    Mat R = Vt.t() * U.t();

    for (auto & sourcePoint : sourcePoints)
    {
        Mat point(3, 1, CV_32F);
        point.at<float>(0, 0) = sourcePoint.x;
        point.at<float>(1, 0) = sourcePoint.y;
        point.at<float>(2, 0) = sourcePoint.z;

        point = R * point;

        sourcePoint = Point3f(point.at<float>(0, 0), point.at<float>(1, 0), point.at<float>(2, 0));
    }
}

void icp(vector<Point3f>& sourcePoints, vector<Point3f>& targetPoints) {
    vector<int> pointCorrespondences;
    float error;
    float lastError;

    struct kdtree* tree = createKDTree(targetPoints);

    cout << "Starting icp with " << ITERATIONS << " iterations" << endl;

    cout << "Finding closest points" << endl;
    findClosestPoints(sourcePoints, targetPoints, tree, pointCorrespondences, error);
    lastError = error;
    cout << "Found closest points, initial error: " << error << endl;
    for (int i = 0; i < ITERATIONS; i++) {
        cout << "Iteration number: " << i << endl;

        icpStep2(sourcePoints, targetPoints, pointCorrespondences);
        cout << "Transformed point sets" << endl;

        cout << "Finding closest points" << endl;
        findClosestPoints(sourcePoints, targetPoints, tree, pointCorrespondences, error);
        cout << "Found closest points" << endl;
        cout << "Error for iteration number " << i << ": " << error << endl;
        if ((lastError - error) < ERROR_DIFFERENCE_THRESHOLD)
        {
            cout << "Error difference is below threshold, returning" << endl;
            break;
        }
        lastError = error;
    }
    kd_free(tree);
}

int main(int argc, char* argv[]) {
    vector<Point3f> sourcePoints, targetPoints;
    pair<string, string> filePair = make_pair("assets/pc0.xyz", "assets/pc1.xyz");
    readFiles(filePair, sourcePoints, targetPoints);

    icp(sourcePoints, targetPoints);

    ofstream file;
    file.open("output/output_pc0-1.xyz");
    for (auto & sourcePoint : sourcePoints) {
        file << sourcePoint.x << " "
             << sourcePoint.y << " "
             << sourcePoint.z << "\n";
    }
    file.close();

    return 0;
}