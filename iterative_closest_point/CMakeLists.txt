cmake_minimum_required(VERSION 3.10)
project(iterative_closest_point)

set(CMAKE_CXX_STANDARD 14)

include_directories(kdtree)
add_subdirectory(kdtree)

add_executable(iterative_closest_point iterative_closest_point.cpp)

find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )
target_link_libraries( iterative_closest_point ${OpenCV_LIBS} kdtree )
